![build status](https://gitlab.com/apiazza134/numerical-cpp/badges/master/pipeline.svg)

# Numerical C++ Docker image

Docker image based on [gcc:latest](https://hub.docker.com/_/gcc) with `cmake` and the following preinstalled libraries
- [Eigen](http://eigen.tuxfamily.org/): `libeigen3-dev`
- [Armadillo](http://arma.sourceforge.net/): `libarmadillo-dev`
- [Spectra](https://spectralib.org/): from source master branch on [github](https://github.com/yixuan/spectra/)
