FROM gcc:latest

RUN apt-get update -qq && \
        apt-get install -qq -q \
        cmake libeigen3-dev clang-format libarmadillo-dev

RUN mv /usr/include/eigen3/Eigen /usr/include/Eigen
RUN mv /usr/include/eigen3/unsupported /usr/include/unsupported
RUN rm -r /usr/include/eigen3

RUN git clone https://github.com/yixuan/spectra.git
RUN cp -r /spectra/include /usr/
# RUN mkdir -p spectra/build
# WORKDIR /spectra/build
# RUN cmake .. -DBUILD_TESTS=TRUE -DCMAKE_INSTALL_PREFIX=/usr
# RUN make all && make test && make install

# WORKDIR /
RUN rm -rf /spectra
RUN apt-get --purge remove -y .\*-doc$ 1> /dev/null 2>&1 \
    && apt-get clean -y 1> /dev/null 2>&1
